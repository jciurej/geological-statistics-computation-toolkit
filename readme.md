The Geological Toolkit is a toolkit developed by the University of Illinois 
to facilitate the computation of geological statistics for large data segments
in an efficient manner by using graphics processing.
